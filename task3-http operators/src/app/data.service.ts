import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { filter, map } from 'rxjs/operators';

@Injectable()
export class DataService {

  constructor(private http: HttpClient) { }

  getUsers() {
    const uri = "https://jsonplaceholder.typicode.com/users";
     this.http.get(uri).pipe(
       map((n:any, index) => {
         let data = [];
         n.forEach( (element) => {
           if(element['name'].length<15)
           {
             data.push(element) ;
            //  console.log(index);
           }

            // n;
         });
        //  n=data;
        // console.log(data);

         return data;
       }),
     ).subscribe(
       response => console.log(response)

     );
  }
}
