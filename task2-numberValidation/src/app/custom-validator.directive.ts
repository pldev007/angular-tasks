import { Directive, ElementRef, OnInit, HostListener } from '@angular/core';
import { ValidatorPipe } from './validator.pipe';

@Directive({
  selector: '[appCustomValidator]',
  providers: [ValidatorPipe]
})
export class CustomValidatorDirective implements OnInit {

  private el: HTMLInputElement;

  constructor(private elRef: ElementRef, private validatorPipe: ValidatorPipe) {
    this.el = this.elRef.nativeElement;
   }

  ngOnInit() {
  }

  @HostListener('keyup', ['$event']) onType(event) {
      this.el.value = this.validatorPipe.transform(this.el.value, event); // opossite of transform
  }
}
