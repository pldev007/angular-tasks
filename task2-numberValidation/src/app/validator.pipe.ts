import { PipeTransform, Pipe } from "@angular/core";
@Pipe({
  name: 'validator'
})
export class ValidatorPipe implements PipeTransform {
  transform(value: any, event: any) {
    if(event.code=='Backspace' && value[value.length-1]==' ') {
      return value = value.substr(0, value.length-1);
    }
    // return value.substr(0,10);
    // let regex = /^\s+$/;
    let newVal = value.replace(/[^0-9\s]/g, "");
    console.log(newVal.length);
    if(newVal.length==3 || newVal.length==7 )
    {
      newVal = newVal + ' ';
    }
    return newVal.substr(0,12);
  }

  parse(value: string) {
    return value;
  }
}
