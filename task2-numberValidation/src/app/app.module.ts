import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { CustomValidatorDirective } from './custom-validator.directive';
import { ValidatorPipe } from './validator.pipe';


@NgModule({
  declarations: [
    AppComponent,
    CustomValidatorDirective,
    ValidatorPipe
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
