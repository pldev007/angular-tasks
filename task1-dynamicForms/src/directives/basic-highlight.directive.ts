import { Directive, ElementRef, OnInit, Renderer2, HostListener } from "@angular/core";

@Directive({
  selector: '[appBasicHighlight]'
})
export class BasicHighlightDirective implements OnInit {
  constructor(private elementRef: ElementRef, private renderer: Renderer2) {}

  ngOnInit() {
  }
    @HostListener('mouseenter') mouseover( event: Event) {
      this.renderer.setStyle(this.elementRef.nativeElement, 'backgroundColor', 'green');
    }


}
