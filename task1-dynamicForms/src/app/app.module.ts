import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {  ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { BasicHighlightDirective } from '../directives/basic-highlight.directive';
import { TestComponent } from './test/test.component';
import { RouterModule, Routes } from '@angular/router';
import { FormComponent } from './form/form.component';
import { TestService } from './test.service';

const appRoutes: Routes = [
  { path:'', component: TestComponent },
  { path:'form', component: FormComponent },
  { path:'app', component: AppComponent }
]

@NgModule({
  declarations: [
    AppComponent,
    BasicHighlightDirective,
    TestComponent,
    FormComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,

    ReactiveFormsModule
  ],
  providers: [TestService],
  bootstrap: [AppComponent]
})
export class AppModule { }
