import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { Subject } from 'rxjs/Subject';
import { Router, ActivatedRoute } from '@angular/router';
import { TestService } from './test.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy{
constructor( private testService: TestService) {}
subscription: Subscription;

ngOnInit() {
  // this.subscription = this.testService.mySubject.subscribe(data => console.log(data));
}

ngOnDestroy() {
  // this.subscription.unsubscribe();
}

}
