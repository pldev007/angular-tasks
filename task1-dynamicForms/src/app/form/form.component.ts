import { Component, OnInit } from '@angular/core';
import { TestService } from '../test.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  constructor(private testService: TestService) { }
  submittedForm =  [];
  ngOnInit() {
    // this.testService.mySubject
    //   .subscribe(
    //     (formArray) => {
    //       this.submittedForm = formArray;
    //       console.log(this.submittedForm);
    //     }
    //   ) this.testService.mySubject
    this.testService.mySubject
    .subscribe(
      (data => console.log(data))
    );


  }

  doThis() {
    this.testService.mySubject
    .subscribe(
      (data => console.log(data))
    );
  }

}
