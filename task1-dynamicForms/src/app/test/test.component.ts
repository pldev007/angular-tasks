import { Component, OnInit, OnDestroy } from '@angular/core';

import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { TestService } from '../test.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

  title = 'app';
  customForm: FormGroup;
  mySubForms = new FormArray([]);
  subscription: Subscription;

  constructor(private router: Router, private route: ActivatedRoute, private testService: TestService) {}

  ngOnInit() {
    this.formInit();
  }

  formInit() {
    this.customForm = new FormGroup({
        "subForms": new FormArray([
          new FormGroup({
            "name": new FormControl(null, Validators.required),
            "hobby": new FormControl(null, Validators.required)
          })
        ])
    });
  }

  onAddForm() {
    (<FormArray>this.customForm.get('subForms')).push(
      new FormGroup({
        "name": new FormControl(null, Validators.required),
        "hobby": new FormControl(null, Validators.required)
      })
    );
  }




  onSubmit() {

    localStorage.setItem('formData',JSON.stringify(this.customForm.value.subForms.slice()))
    // this.testService.mySubject.next(this.customForm.value.subForms.slice());

    // this.router.navigate(['form']);

  }

  onUpdate() {
    let data = localStorage.getItem('formData');
    let myFormData =  JSON.parse(data);
    console.log(myFormData);
    this.customForm.controls['subForms'] = new FormArray([]);
    myFormData.forEach(
      (value) => {

        let controls = {}
        Object.keys(value).forEach(v => {
          controls[v] = new FormControl(value[v], Validators.required)
        });

        (<FormArray>this.customForm.get('subForms')).push(
          new FormGroup(controls)
        )

      }
    );

  }

  // ngOnDestroy() {
  //   this.subscription.unsubscribe();
  // }

}
